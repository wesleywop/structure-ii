package NetFlix;

import java.util.Map;
import java.util.HashMap;
import NetFlix.Helpers.Validator;

public class Person {

    private final String NAME;
    private final String CPF;
    private final String PHONE;
    private final PersonType PERSONTYPE;
    private final Map<String, Title> TITLES;

    public Person(String name, String cpf, String phone, PersonType personType) {
        NAME = Validator.capitalize(name);
        CPF = cpf;
        PHONE = phone;
        PERSONTYPE = personType;
        TITLES = new HashMap<>();
    }

    public String getNAME() {
        return NAME;
    }

    public String getCPF() {
        return CPF;
    }

    public int getTitlesSize() {
        return TITLES.size();
    }

    public Title addTitle(Title title) {
        Title isTitle = TITLES.put(title.getNAME(), title);
        return isTitle != null ? isTitle : title;
    }

    @Override
    public String toString() {
        return "Nome => " + NAME + ", CPF => " + CPF + ", Tipo => " + Validator.capitalize(PERSONTYPE.toString()) + ", Títulos => " + TITLES.keySet();
    }
}
