package NetFlix;

import java.util.Map;
import java.util.HashMap;
import NetFlix.Helpers.Validator;

public class NetFlix {

    private final Map<String, Category> CATEGORIES;
    private final Map<String, SubCategory> SUBCATEGORIES;
    private final Map<String, Title> TITLES;
    private final Map<String, Person> PERSONS;

    public NetFlix() {
        CATEGORIES = new HashMap<>();
        SUBCATEGORIES = new HashMap<>();
        TITLES = new HashMap<>();
        PERSONS = new HashMap<>();
    }

    public int getCategoriesSize() {
        return CATEGORIES.size();
    }

    public int getSubCategoriesSize() {
        return SUBCATEGORIES.size();
    }

    public int getTitlesSize() {
        return TITLES.size();
    }

    public int getPersonsSize() {
        return PERSONS.size();
    }

    public Category getCategory(String name) {
        return CATEGORIES.get(Validator.capitalize(name));
    }

    public Category addCategory(Category category) {
        Category isCategory = CATEGORIES.put(category.getNAME(), category);
        return isCategory != null ? isCategory : category;
    }

    public SubCategory getSubCategory(String name) {
        return SUBCATEGORIES.get(Validator.capitalize(name));
    }

    public SubCategory addSubCategory(SubCategory subCategory) {
        SubCategory isSubCategory = SUBCATEGORIES.put(subCategory.getNAME(), subCategory);
        return isSubCategory != null ? isSubCategory : subCategory;
    }

    public Title getTitle(String name) {
        return TITLES.get(Validator.capitalize(name));
    }

    public Title addTitle(Title title) {
        Title isTitle = TITLES.put(title.getNAME(), title);
        return isTitle != null ? isTitle : title;
    }

    public Person getPerson(String cpf) {
        return PERSONS.get(cpf);
    }

    public Person addPerson(Person person) {
        Person isPerson = PERSONS.put(person.getCPF(), person);
        return isPerson != null ? isPerson : person;
    }
}
