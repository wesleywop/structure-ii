package NetFlix.Helpers;

import java.util.Scanner;

public class Keyboard {

    private static final Scanner input = new Scanner(System.in);

    public static int readInt(String text) {
        boolean ok;
        int value = 0;

        do {
            System.out.print(text);
            try {
                ok = true;
                value = input.nextInt();
            } catch (Exception exception) {
                System.err.println("Valor inválido!");
                ok = false;
            }

        } while (!ok);
        return value;
    }

    public static float readFloat(String text) {
        boolean ok;
        float value = 0;

        do {
            System.out.print(text);
            try {
                ok = true;
                value = input.nextFloat();
            } catch (Exception exception) {
                System.err.println("Valor inválido!");
                ok = false;
            }

        } while (!ok);
        return value;
    }

    public static double readDouble(String text) {
        boolean ok;
        double value = 0;

        do {
            System.out.print(text);
            try {
                ok = true;
                value = input.nextDouble();
            } catch (Exception exception) {
                System.err.println("Valor inválido!");
                ok = false;
            }

        } while (!ok);
        return value;
    }

    public static long readLong(String text) {
        boolean ok;
        long value = 0;

        do {
            System.out.print(text);
            try {
                ok = true;
                value = input.nextLong();
            } catch (Exception exception) {
                System.err.println("Valor inválido!");
                ok = false;
            }

        } while (!ok);
        return value;
    }

    public static String readString(String text) {
        boolean ok;
        String value = null;

        do {
            System.out.print(text);
            try {
                ok = true;
                value = input.nextLine();
            } catch (Exception exception) {
                System.err.println("Valor inválido!");
                ok = false;
            }

        } while (!ok);
        return value;
    }

    public static byte readByte(String text) {
        boolean ok;
        byte value = 0;

        do {
            System.out.print(text);
            try {
                ok = true;
                value = input.nextByte();
                input.nextLine();
            } catch (Exception exception) {
                System.err.println("Valor inválido!");
                ok = false;
            }

        } while (!ok);
        return value;
    }

    public static char readChar(String text) {
        boolean ok;
        char value = ' ';

        do {
            System.out.print(text);
            try {
                ok = true;
                value = input.next().charAt(0);
            } catch (Exception exception) {
                System.err.println("Valor inválido!");
                ok = false;
            }

        } while (!ok);
        return value;
    }

    public static int readShort(String text) {
        boolean ok;
        short value = 0;

        do {
            System.out.print(text);
            try {
                ok = true;
                value = input.nextShort();
            } catch (Exception exception) {
                System.err.println("Valor inválido!");
                ok = false;
            }

        } while (!ok);
        return value;
    }

    public static String readData(String text) {
        boolean ok;
        String value = null;

        do {
            System.out.print(text);
            try {
                ok = true;
                value = input.nextLine();
            } catch (Exception exception) {
                System.err.println("Valor inválido!");
                ok = false;
            }

        } while (!ok);
        return value;
    }

    // -----------------------------------------------------------------
    // Creating of a menu. Each item of menu should be separated each other by a
    // slash (/)
    // -----------------------------------------------------------------
    public static int menu(String options) {
        int p;
        String option;
        int nOptions = 0;        
        do {
            p = options.indexOf('/');
            if (p == -1) {
                nOptions++;
                System.out.println(nOptions + " => " + options);
                continue;
            }
            option = options.substring(0, p);
            nOptions++;
            System.out.println(nOptions + " => " + option);
            options = options.substring(p + 1);

        } while (p > 0);

        p = readInt("\nSelecione uma das opções acima: ");

        while (p < 1 || p > nOptions) {
            System.out.println("Opção inválida.");
            p = readInt("Selecione uma opção: ");
        }
        
        input.nextLine();

        return p;
    }

    public static void waitEnter() {
        byte[] bytes = new byte[160];
        System.out.print("\nPressione Enter para continuar");
        try {
            System.in.read(bytes);
        } catch (Exception e) {
            System.out.println("Erro: " + e);
        }
    }

    public static void clrscr() {
        for (int i = 1; i < 100; i++) {
            System.out.println();
        }
    }
}