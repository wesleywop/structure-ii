package NetFlix.Helpers;

// import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Arrays;
import java.util.stream.Collectors;
// import org.apache.commons.lang3.text.WordUtils;

public class Validator {

    public static String checkCpf(String cpf) {
        String msg = "";
        String cpfReplaced = cpf.replaceAll("[.\\- ]", "");

        if (!isCorrect(cpf)) {
            msg = "O CPF deve conter apenas números.";
        } else if (hasTheSameDigits(cpfReplaced)) {
            msg = "Todos os dígitos do CPF são iguais.";
        } else if (!isDigit(cpfReplaced)) {
            msg = "CPF inválido.";
        }
        return msg;
    }

    private static boolean isCorrect(String cpf) {
        return cpf.matches("^(\\d{11})$");
    }

    public static boolean hasTheSameDigits(String cpf) {
        return cpf.matches("^(\\d)(\\1){10}$");
    }

    private static boolean isDigit(String cpf) {
        char dig10, dig11;
        int sm, i, r, num, weight;

        // Calculate the 1st. check digit
        sm = 0;
        weight = 10;
        for (i = 0; i < 9; i++) {
            // Convert the n th character of CPF in a number,
            // for example, transform the character '0' in an Integer 0
            // 48 is the position of '0' in the table ASCII
            num = cpf.charAt(i) - 48;
            sm += num * weight;
            weight -= 1;
        }

        r = 11 - (sm % 11);
        dig10 = (r == 10 || r == 11) ? '0' : (char) (r + 48);

        // Calculate the 2nd. check digit
        sm = 0;
        weight = 11;
        for (i = 0; i < 10; i++) {
            num = cpf.charAt(i) - 48;
            sm += num * weight;
            weight -= 1;
        }

        r = 11 - (sm % 11);
        dig11 = (r == 10 || r == 11) ? '0' : (char) (r + 48);

        // Check whether the calculated digits with digits give informed
        return dig10 == cpf.charAt(9) && dig11 == cpf.charAt(10);
    }

    public static boolean checkPhone(String phone) {
        return phone.matches("^\\(?[11-99]{2}\\)?[9]\\d{4}-?\\d{4}$");
    }

    public static String capitalize(String word) {
        return Arrays.stream(word.split(" ")).map(w -> Pattern.compile("\\b([d|D][a-zA-Z]{1,2})\\b").
                matcher(w).find() ? w.toLowerCase() : Character.toUpperCase(w.charAt(0)) + w.substring(1).toLowerCase()).collect(Collectors.joining(" "));
    }

    /*private static String capitalizeWordUtils(String word) {
     String newWord = WordUtils.capitalizeFully(word);
     Matcher matcher = Pattern.compile("([d|D][a-zA-Z]{1,2})").matcher(newWord);
     while (matcher.find()) {
     String match = matcher.group(0);
     newWord = newWord.replaceAll(match, match.toLowerCase());
     }
     return newWord;
     }*/
    private static String capitalizeFor(String word) {
        word = word.trim();
        String newWord = "";

        String words[] = word.split(" ");
        for (int i = 0; i < words.length; i++) {
            if (words[i].matches("^(d|D)[a-zA-Z]{1,2}$")) {
                words[i] = words[i].toLowerCase();
            } else {
                words[i] = words[i].substring(0, 1).toUpperCase().concat(words[i].substring(1).toLowerCase());
            }
            newWord += words[i];
            if (i < words.length - 1) {
                newWord += " ";
            }
        }
        return newWord;
    }
}
