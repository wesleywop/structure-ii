package NetFlix;

import java.util.stream.Collectors;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import NetFlix.Helpers.Validator;

public class Category {

    private final String NAME;
    private final Map<String, Title> TITLES;
    private final Map<String, SubCategory> SUBCATEGORIES;

    public Category(String name) {
        NAME = Validator.capitalize(name);
        TITLES = new HashMap<>();
        SUBCATEGORIES = new HashMap<>();
    }

    public String getNAME() {
        return NAME;
    }

    public int getSubCategoriesSize() {
        return SUBCATEGORIES.size();
    }

    public int getTitlesSize() {
        return TITLES.size();
    }

    public List<Title> getTitlesByRating(byte minimum, String order) {
        List<Title> orderedTitles = new ArrayList(TITLES.values().stream().filter(title -> title.getRATING() >= minimum).collect(Collectors.toList()));
        if (order.equalsIgnoreCase("c")) {
            Collections.sort(orderedTitles);
        } else {
            Collections.sort(orderedTitles, Collections.reverseOrder());
        }
        return orderedTitles;
    }

    public Title addTitle(Title title) {
        Title isTitle = TITLES.put(title.getNAME(), title);
        return isTitle != null ? isTitle : title;
    }

    public SubCategory getSubCategory(String name) {
        return SUBCATEGORIES.get(name);
    }

    public SubCategory addSubCategory(SubCategory subCategory) {
        SubCategory isSubCategory = SUBCATEGORIES.put(subCategory.getNAME(), subCategory);
        return isSubCategory != null ? isSubCategory : subCategory;
    }

    @Override
    public String toString() {
        return "Nome => " + NAME + ", Títulos => " + TITLES.keySet() + ", Subcategorias => " + SUBCATEGORIES.keySet();
    }
}
