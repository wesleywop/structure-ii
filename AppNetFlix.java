package NetFlix;

import NetFlix.Helpers.Keyboard;
import NetFlix.Helpers.Validator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AppNetFlix {

    private static final NetFlix NETFLIX = new NetFlix();

    public static void main(String[] args) throws Exception {
        init();
        app();
    }

    private static void init() throws Exception {

        /* --------------------------- TITLES --------------------------- */
        Title movie1 = new Title("Filme 1", "Filme mais ou menos", (byte) 3, TitleType.FILME);
        Title movie2 = new Title("Filme 2", "Filme bom", (byte) 5, TitleType.FILME);
        Title serie1 = new Title("Série 1", "Série mais ou menos", (byte) 2, TitleType.SÉRIE);

        /* --------------------------- PERSONS --------------------------- */
        Person actor1 = new Person("ator josé dAs sIlVa DoS anJoS", "77777777777", "(77)7777-8888", PersonType.ATOR);
        Person actor2 = new Person("ator lowercase", "88888888888", "(88)8888-8888", PersonType.ATOR);
        Person principal1 = new Person("Diretor UPPERCASE", "99999999999", "(99)9999-9999", PersonType.DIRETOR);

        /* --------------------------- CATEGORIES --------------------------- */
        Category category1 = new Category("Ação");
        Category category2 = new Category("Comédia");
        Category category3 = new Category("Drama");

        /* --------------------------- SUBCATEGORIES --------------------------- */
        SubCategory subCategory1 = new SubCategory("Sub-Ação");
        SubCategory subCategory2 = new SubCategory("Sub-Comédia");
        SubCategory subCategory3 = new SubCategory("Sub-Drama");

        /* --------------------------- LISTS --------------------------- */
        actor1.addTitle(movie1);
        actor2.addTitle(movie2);
        principal1.addTitle(serie1);

        movie1.addPerson(actor1);
        movie1.addPerson(actor2);
        movie1.addPerson(principal1);
        movie2.addPerson(actor2);
        serie1.addPerson(principal1);

        movie1.addCategory(category1);
        movie2.addCategory(category2);
        serie1.addCategory(category3);

        movie1.addSubCategory(subCategory1);
        movie1.addSubCategory(subCategory2);
        movie2.addSubCategory(subCategory2);
        serie1.addSubCategory(subCategory3);

        category1.addTitle(movie1);
        category2.addTitle(movie2);
        category3.addTitle(serie1);

        category1.addSubCategory(subCategory1);
        category2.addSubCategory(subCategory2);
        category3.addSubCategory(subCategory3);

        subCategory1.addCategory(category1);
        subCategory2.addCategory(category2);
        subCategory3.addCategory(category3);

        subCategory1.addTitle(movie1);
        subCategory2.addTitle(movie1);
        subCategory2.addTitle(movie2);
        subCategory3.addTitle(serie1);

        NETFLIX.addTitle(movie1);
        NETFLIX.addTitle(movie2);
        NETFLIX.addTitle(serie1);

        NETFLIX.addPerson(actor1);
        NETFLIX.addPerson(actor2);
        NETFLIX.addPerson(principal1);

        NETFLIX.addCategory(category1);
        NETFLIX.addCategory(category2);
        NETFLIX.addCategory(category3);

        NETFLIX.addSubCategory(subCategory1);
        NETFLIX.addSubCategory(subCategory2);
        NETFLIX.addSubCategory(subCategory3);
    }

    private static void app() throws Exception {
        int option;

        System.out.println("Seja bem-vindo(a) ao Sistema NetFlix!!");

        do {
            System.out.println();
            option = Keyboard.menu("Sair/Cadastrar título/Cadastrar pessoa/Cadastrar categoria/Cadastrar subcategoria/Vincular pessoas a um título"
                    + "/Vincular título a categorias/Vincular título a subcategorias/Vincular subcategorias a categoria/"
                    + "Buscar título/Buscar pessoa/Buscar categoria/Buscar subcategoria/Obter títulos ordenados pela avaliação");
            System.out.println();
            switch (option) {
                case 1:
                    exit();
                    break;
                case 2:
                    addTitle();
                    break;
                case 3:
                    addPerson();
                    break;
                case 4:
                    addCategory();
                    break;
                case 5:
                    addSubCategory();
                    break;
                case 6:
                    addPersonToTitle(null, null);
                    break;
                case 7:
                    addCategoryToTitle(null);
                    break;
                case 8:
                    addSubCategoryToTitle(null);
                    break;
                case 9:
                    addSubCategoryToCategory(null);
                    break;
                case 10:
                    getTitle();
                    break;
                case 11:
                    getPerson();
                    break;
                case 12:
                    getCategory();
                    break;
                case 13:
                    getSubCategory();
                    break;
                case 14:
                    getTitlesOrdered();
                    break;
            }
        } while (option > 1 || option < 15);
    }

    private static void addTitle() {
        System.out.println("Insira as informações abaixo para realizar o cadastro do título:\n");

        String titleName;
        Title titleAux;
        do {
            titleName = Keyboard.readString("Nome do título: ");
            titleAux = NETFLIX.getTitle(titleName);
            if (titleAux != null) {
                System.err.println("\nNome já existente!\n");
            }
        } while (titleAux != null);

        String description = Keyboard.readString("Descrição: ");
        byte rating = validRating("Avaliação: ");
        String type = validCustomRegex("Tipo (Filme/Série): ", "^(filme|s[eé]rie)$");

        Title title = new Title(titleName, description, rating, TitleType.valueOf(type.toUpperCase()));

        if (NETFLIX.addTitle(title) == title) {
            Category category;
            boolean regex = false;
            do {
                category = addCategoryToTitle(title);
                if (category != null) {
                    regex = validCustomRegex("\nDeseja vincular mais alguma categoria este título?(s/n): ", "^(s|n)$").equals("s");
                }
            } while (regex && category != null);

            SubCategory subCategory = null;
            do {
                regex = validCustomRegex("\nDeseja vincular subcategorias à este título?(s/n): ", "^(s|n)$").equals("s");
                if (regex) {
                    subCategory = addSubCategoryToTitle(title);
                }
            } while (regex && subCategory != null);

            Person person = null;
            do {
                regex = validCustomRegex("\nDeseja vincular pessoas á este título?(s/n): ", "^(s|n)$").equals("s");
                if (regex) {
                    person = addPersonToTitle(title, null);
                }
            } while (regex && person != null);

            System.out.println("\nTitulo '" + titleName + "' cadastrado com sucesso!");
        } else {
            System.err.println("\nTitulo já existente!");
        }
    }

    private static void addPerson() {
        System.out.println("Insira as informações abaixo para realizar o cadastro do usuário:\n");

        String name = Keyboard.readString("Nome: ");

        String cpf;
        String msg;
        Person personAux = null;
        do {
            cpf = Keyboard.readString("CPF: ");
            msg = Validator.checkCpf(cpf);
            if (!msg.equals("")) {
                System.err.println("\n" + msg + "\n");
            } else {
                personAux = NETFLIX.getPerson(cpf);
                if (personAux != null) {
                    System.err.println("\nCPF já existente!\n");
                }
            }
        } while (!msg.equals("") || personAux != null);

        String phone;
        boolean valid;
        do {
            phone = Keyboard.readString("Telefone: ");
            valid = Validator.checkPhone(phone);
            if (!valid) {
                System.err.println("\nO telefone não está no formato correto!\n");
            }
        } while (!valid);

        String type = validCustomRegex("Tipo (Ator/Diretor): ", "^(ator|diretor)$");

        Person person = new Person(name, cpf, phone, PersonType.valueOf(type.toUpperCase()));

        if (NETFLIX.addPerson(person) == person) {
            personAux = null;
            boolean regex;
            do {
                regex = validCustomRegex("\nDeseja vincular títulos à esta pessoa?(s/n): ", "^(s|n)$").equals("s");
                if (regex) {
                    personAux = addPersonToTitle(null, person);
                }
            } while (regex && personAux != null);

            System.out.println("\nPessoa '" + person.getNAME() + "' cadastrada com sucesso!");
        } else {
            System.err.println("\nPessoa já existente!");
        }
    }

    private static void addCategory() {
        System.out.println("Insira as informações abaixo para realizar o cadastro da categoria:\n");

        String categoryName;
        Category categoryAux;
        do {
            categoryName = Keyboard.readString("Nome: ");
            categoryAux = NETFLIX.getCategory(categoryName);
            if (categoryAux != null) {
                System.err.println("\nNome já existente!\n");
            }
        } while (categoryAux != null);

        Category category = new Category(categoryName);

        if (NETFLIX.addCategory(category) == category) {
            SubCategory subCategory = null;
            boolean regex;
            do {
                regex = validCustomRegex("\nDeseja vincular subcategorias à esta categoria?(s/n): ", "^(s|n)$").equals("s");
                if (regex) {
                    subCategory = addSubCategoryToCategory(category);
                }
            } while (regex && subCategory != null);
            System.out.println("\nCategoria '" + categoryName + "' cadastrada com sucesso!");
        } else {
            System.err.println("\nCategoria já existente!");
        }
    }

    private static void addSubCategory() {
        System.out.println("\nInsira as informações abaixo para realizar o cadastro da subcategoria:\n");

        Category categoryAux = validCategory();

        String subCategoryName;
        SubCategory subCategoryAux;
        do {
            subCategoryName = Keyboard.readString("Nome da subcategoria: ");
            subCategoryAux = NETFLIX.getSubCategory(subCategoryName);
            if (subCategoryAux != null) {
                System.err.println("\nNome já existente!\n");
            }
        } while (subCategoryAux != null);

        SubCategory subCategory = new SubCategory(subCategoryName);

        if (categoryAux.addSubCategory(subCategory) == subCategory) {
            NETFLIX.addSubCategory(subCategory);
            subCategory.addCategory(categoryAux);
            System.out.println("\nSubcategoria '" + subCategory.getNAME() + "' cadastrada com sucesso!");
        } else {
            System.err.println("\nSubcategoria já existente!");
        }
    }

    private static Person addPersonToTitle(Title title, Person person) {
        Title titleAux = title != null ? title : validTitle();
        String titleName = titleAux.getNAME();
        Person personAux = null;
        if (titleAux.getPersonsSize() < NETFLIX.getPersonsSize()) {
            personAux = person != null ? person : validPerson();
            String personName = personAux.getNAME();
            if (personAux.getTitlesSize() < NETFLIX.getTitlesSize()) {
                if (titleAux.getPerson(personAux.getCPF()) == null) {
                    titleAux.addPerson(personAux);
                    personAux.addTitle(titleAux);
                    System.out.println("\nPessoa '" + personName + "' vinculada ao título '" + titleName + "' com sucesso!");
                    if (!Thread.currentThread().getStackTrace()[2].getMethodName().equalsIgnoreCase("addTitle")
                            && validCustomRegex("\nDeseja vincular mais pessoas a este título?(s/n): ", "^(s|n)$").equals("s")) {
                        addPersonToTitle(titleAux, null);
                    }
                } else {
                    System.err.println("\nTítulo '" + titleName + "' já vinculado à pessoa '" + personName + "'!");
                }
            } else {
                System.err.println("\nPessoa '" + personName + "' já vinculada à todos os títulos disponíveis!");
            }
        } else {
            System.err.println("\nTítulo '" + titleName + "' já vinculado à todas as pessoas disponíveis!");
        }
        return personAux;
    }

    private static Category addCategoryToTitle(Title title) {
        Title titleAux = title != null ? title : validTitle();
        String titleName = titleAux.getNAME();
        Category category = null;
        if (titleAux.getCategoriesSize() < NETFLIX.getCategoriesSize()) {
            category = validCategory();
            String categoryName = category.getNAME();
            if (category.getTitlesSize() < NETFLIX.getTitlesSize()) {
                if (titleAux.getCategory(categoryName) == null) {
                    titleAux.addCategory(category);
                    category.addTitle(titleAux);
                    System.out.println("\nCategoria '" + categoryName + "' vinculada ao título '" + titleName + "' com sucesso!");
                    if (!Thread.currentThread().getStackTrace()[2].getMethodName().equalsIgnoreCase("addTitle")
                            && validCustomRegex("\nDeseja vincular mais categorias a este título?(s/n): ", "^(s|n)$").equals("s")) {
                        addCategoryToTitle(titleAux);
                    }
                } else {
                    System.err.println("\nTítulo '" + titleName + "' já vinculado à categoria '" + categoryName + "'!");
                }
            } else {
                System.err.println("\nCategoria '" + categoryName + "' já vinculada à todos os títulos disponíveis!");
            }
        } else {
            System.err.println("\nTítulo '" + titleName + "' já vinculado à todas as categorias disponíveis!");
        }
        return category;
    }

    private static SubCategory addSubCategoryToTitle(Title title) {
        Title titleAux = title != null ? title : validTitle();
        String titleName = titleAux.getNAME();
        SubCategory subCategory = null;
        if (titleAux.getSubCategoriesSize() < NETFLIX.getSubCategoriesSize()) {
            subCategory = validSubCategory();
            String subCategoryName = subCategory.getNAME();
            if (subCategory.getTitlesSize() < NETFLIX.getTitlesSize()) {
                if (titleAux.getSubCategory(subCategoryName) == null) {
                    titleAux.addSubCategory(subCategory);
                    subCategory.addTitle(titleAux);
                    System.out.println("\nSubcategoria '" + subCategoryName + "' vinculada ao título '" + titleName + "' com sucesso!");
                    if (!Thread.currentThread().getStackTrace()[2].getMethodName().equalsIgnoreCase("addTitle")
                            && validCustomRegex("\nDeseja vincular mais subcategorias a este título?(s/n): ", "^(s|n)$").equals("s")) {
                        addSubCategoryToTitle(titleAux);
                    }
                } else {
                    System.err.println("\nTítulo '" + titleName + "' já vinculado à subcategoria '" + subCategoryName + "'!");
                }
            } else {
                System.err.println("\nSubcategoria '" + subCategoryName + "' já vinculada à todos os títulos disponíveis!");
            }
        } else {
            System.err.println("\nTítulo '" + titleName + "' já vinculado à todas as subcategorias disponíveis!");
        }
        return subCategory;
    }

    private static SubCategory addSubCategoryToCategory(Category category) {
        Category categoryAux = category != null ? category : validCategory();
        String categoryName = categoryAux.getNAME();
        SubCategory subCategory = null;
        if (categoryAux.getSubCategoriesSize() < NETFLIX.getSubCategoriesSize()) {
            subCategory = validSubCategory();
            String subCategoryName = subCategory.getNAME();
            if (subCategory.getCategoriesSize() < NETFLIX.getCategoriesSize()) {
                if (categoryAux.getSubCategory(subCategoryName) == null) {
                    categoryAux.addSubCategory(subCategory);
                    subCategory.addCategory(categoryAux);
                    System.out.println("\nSubcategoria '" + subCategoryName + "' vinculada à categoria com sucesso!");
                    if (!Thread.currentThread().getStackTrace()[2].getMethodName().equalsIgnoreCase("addCategory")
                            && validCustomRegex("\nDeseja vincular mais subcategorias a esta categoria?(s/n): ", "^(s|n)$").equals("s")) {
                        addSubCategoryToCategory(categoryAux);
                    }
                } else {
                    System.err.println("\nCategoria '" + categoryName + "' já vinculada à subcategoria '" + subCategoryName + "'!");
                }
            } else {
                System.err.println("\nSubcategoria '" + subCategoryName + "' já vinculada à todas as categorias disponíveis!");
            }
        } else {
            System.err.println("\nCategoria '" + categoryName + "' já vinculada à todas as subcategorias disponíveis!");
        }
        return subCategory;
    }

    private static void getTitle() {
        Title title = NETFLIX.getTitle(Keyboard.readString("Nome: "));
        if (title != null) {
            System.out.println("\n" + title);
        } else {
            System.err.println("\nTítulo não encontrado!");
        }
    }

    private static void getPerson() {
        Person person = NETFLIX.getPerson(Keyboard.readString("CPF: "));
        if (person != null) {
            System.out.println("\n" + person);
        } else {
            System.err.println("\nPessoa não encontrada!");
        }
    }

    private static void getCategory() {
        Category category = NETFLIX.getCategory(Keyboard.readString("Nome: "));
        if (category != null) {
            System.out.println("\n" + category);
        } else {
            System.err.println("\nCategoria não encontrada!");
        }
    }

    private static void getSubCategory() {
        SubCategory subCategory = NETFLIX.getSubCategory(Keyboard.readString("Nome: "));
        if (subCategory != null) {
            System.out.println("\n" + subCategory);
        } else {
            System.err.println("\nSubcategoria não encontrada!");
        }
    }

    private static void getTitlesOrdered() {
        System.out.println("Insira as informações abaixo para obter os títulos da categoria desejada de forma ordenada:\n");

        Category category = validCategory();
        if (category.getTitlesSize() > 0) {
            byte minimum = validRating("Número mínimo de avaliações: ");
            List<Title> titles = category.getTitlesByRating(minimum,
                    validCustomRegex("Insira a ordem ('c' para crescente ou 'd' para decrescente): ", "^(c|d)$"));
            if (titles.size() > 0) {
                System.out.print("\nCategoria\t\tTítulo\t\t\tAvaliação\n" + category.getNAME());
                titles.stream().map(t -> "\t\t\t" + t.getNAME() + "\t\t\t" + t.getRATING()).forEach(System.out::println);
            } else {
                System.err.println("\nEsta categoria não possui nenhum título com o mínimo de avaliação '" + minimum + "'!");
            }
        } else {
            System.err.println("\nEsta categoria não possui nenhum título cadastrado!");
        }

        System.out.println();
    }

    private static void exit() {
        System.out.print("Obrigado e volte sempre!");
        System.exit(0);
    }

    private static Title validTitle() {
        Title title;
        do {
            title = NETFLIX.getTitle(Keyboard.readString("Nome do título: "));
            if (title == null) {
                System.err.println("\nInsira um título válido para prosseguir!\n");
            }
        } while (title == null);
        return title;
    }

    private static Person validPerson() {
        Person person;
        do {
            person = NETFLIX.getPerson(Keyboard.readString("CPF da pessoa: "));
            if (person == null) {
                System.err.println("\nInsira uma pessoa válida para prosseguir!\n");
            }
        } while (person == null);
        return person;
    }

    private static Category validCategory() {
        Category category;
        do {
            category = NETFLIX.getCategory(Keyboard.readString("Nome da categoria: "));
            if (category == null) {
                System.err.println("\nInsira uma categoria válida para prosseguir!\n");
            }
        } while (category == null);
        return category;
    }

    private static SubCategory validSubCategory() {
        SubCategory subCategory;
        do {
            subCategory = NETFLIX.getSubCategory(Keyboard.readString("Nome da subcategoria: "));
            if (subCategory == null) {
                System.err.println("\nInsira uma subcategoria válida para prosseguir!\n");
            }
        } while (subCategory == null);
        return subCategory;
    }

    private static byte validRating(String msg) {
        byte rating;
        do {
            rating = Keyboard.readByte(msg);
            if (rating < 1 || rating > 5) {
                System.err.println("\nInsira um número entre 1 e 5 para prosseguir!\n");
            }
        } while (rating < 1 || rating > 5);
        return rating;
    }

    private static String validCustomRegex(String msg, String regex) {
        String attribute;
        Matcher matcher;
        boolean finder;
        do {
            attribute = Keyboard.readString(msg);
            matcher = Pattern.compile(regex).matcher(attribute.toLowerCase());
            finder = matcher.find();
            if (!finder){
                System.err.println("\nInsira uma opção válida para prosseguir!\n");
            }
        } while (!finder);
        return matcher.group().toLowerCase();
    }
}
