package NetFlix;

import java.util.Map;
import java.util.HashMap;
import NetFlix.Helpers.Validator;

public class SubCategory {

    private final String NAME;
    private final Map<String, Category> CATEGORIES;
    private final Map<String, Title> TITLES;

    public SubCategory(String name) {
        NAME = Validator.capitalize(name);
        CATEGORIES = new HashMap<>();
        TITLES = new HashMap<>();
    }

    public String getNAME() {
        return NAME;
    }

    public int getCategoriesSize() {
        return CATEGORIES.size();
    }

    public int getTitlesSize() {
        return TITLES.size();
    }

    public Category addCategory(Category category) {
        Category isCategory = CATEGORIES.put(category.getNAME(), category);
        return isCategory != null ? isCategory : category;
    }

    public Map<String, Title> getTITLES() {
        return TITLES;
    }

    public Title addTitle(Title title) {
        Title isTitle = TITLES.put(title.getNAME(), title);
        return isTitle != null ? isTitle : title;
    }

    @Override
    public String toString() {
        return "Nome => " + NAME + ", Categoria => " + CATEGORIES.keySet() + ", Títulos => " + TITLES.keySet();
    }
}
