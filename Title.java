package NetFlix;

import java.util.Map;
import java.util.HashMap;
import NetFlix.Helpers.Validator;

public class Title implements Comparable<Title> {

    private final String NAME;
    private final String DESCRIPTION;
    private final byte RATING;
    private final TitleType TITLETYPE;
    private final Map<String, Category> CATEGORIES;
    private final Map<String, SubCategory> SUBCATEGORIES;
    private final Map<String, Person> PERSONS;

    public Title(String name, String description, byte rating, TitleType titleType) {
        NAME = Validator.capitalize(name);
        DESCRIPTION = Validator.capitalize(description);
        RATING = rating;
        TITLETYPE = titleType;
        CATEGORIES = new HashMap<>();
        SUBCATEGORIES = new HashMap<>();
        PERSONS = new HashMap<>();
    }

    public String getNAME() {
        return NAME;
    }

    public byte getRATING() {
        return RATING;
    }

    public int getCategoriesSize() {
        return CATEGORIES.size();
    }
    public int getSubCategoriesSize() {
        return SUBCATEGORIES.size();
    }

    public int getPersonsSize() {
        return PERSONS.size();
    }

    public Category getCategory(String name) {
        return CATEGORIES.get(Validator.capitalize(name));
    }

    public Category addCategory(Category category) {
        Category isCategory = CATEGORIES.put(category.getNAME(), category);
        return isCategory != null ? isCategory : category;
    }

    public SubCategory getSubCategory(String name) {
        return SUBCATEGORIES.get(Validator.capitalize(name));
    }

    public SubCategory addSubCategory(SubCategory subCategory) {
        SubCategory isSubCategory = SUBCATEGORIES.put(subCategory.getNAME(), subCategory);
        return isSubCategory != null ? isSubCategory : subCategory;
    }

    public Person getPerson(String cpf) {
        return PERSONS.get(cpf);
    }

    public Person addPerson(Person person) {
        Person isPerson = PERSONS.put(person.getCPF(), person);
        return isPerson != null ? isPerson : person;
    }

    @Override
    public int compareTo(Title t) {
        if (RATING > t.RATING) {
            return 1;
        }

        if (RATING < t.RATING) {
            return -1;
        }

        return 0;
    }

    @Override
    public String toString() {
        return "Nome => " + NAME + ", Avaliação => " + RATING + ", Tipo => " + Validator.capitalize(TITLETYPE.toString())
                + ", Categorias => " + CATEGORIES.keySet() + ", Subcategorias => " + SUBCATEGORIES.keySet() + ", Pessoas => " + PERSONS.keySet();
    }
}
